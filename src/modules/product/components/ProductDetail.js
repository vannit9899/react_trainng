import {useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import {getProductById} from "../core/request";
import { Link } from "react-router-dom";
const ProductDetail = () => {
    const {id} = useParams();
    const [product, setProduct] = useState({});


    useEffect(() => {
        getProductById(id).then((response)=>{
            setProduct(response.data);
        })
    }, [id]);

    if (!product.id){
        return  <div className="text-center fs-6">
            Loading...
        </div>
    }

    return <div className="container mt-5">
        <div className="row">                      
            <div className="col-6">
                <img className="w-100" src={product.thumbnail} alt=""/>
            </div>
            <div className="col-6">
                <h2>{product.title}</h2>
                <p>{product.description}</p>
                <h5>Price: <span className="text-danger">${product.price}</span></h5>
            </div>
        </div>
        <Link to="/"><button className="btn btn-primary mt-2">Back</button></Link>
    </div>
}
export default ProductDetail;